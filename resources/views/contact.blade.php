@extends('layout')

@section('content')

    <article>
        <h1>
            Contact Us!
        </h1>
        <p>
            Contact us for the latest news about Kushal Laravel Project!
        </p>
        <p>
            Vestibulum quis mi quis ipsum egestas consectetur sit amet vitae odio. Quisque malesuada dui eu libero
            vulputate, sed interdum ex ornare. Quisque non mi tristique, laoreet libero eu, dictum ipsum. Nullam vel
            libero congue, auctor velit vel, fringilla erat. Cras nec semper augue. Phasellus vitae odio ornare tortor
            feugiat aliquam rutrum eget justo. Sed interdum fringilla mauris, dictum sollicitudin lectus condimentum at.
        </p>
        <form>
            <fieldset name="name">
                <label for="inputName">
                    Name
                </label>
                <input name="inputName" id="inputName" autofocus="autofocus"/>
            </fieldset>
        </form>
    </article>
@endsection
