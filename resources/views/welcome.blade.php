@extends('layout')

@section('content')

    @foreach($tasks as $task)
        {{ ucfirst($task) }}.
    @endforeach

    <article>
        <h1>
            Welcome!
        </h1>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eu mauris finibus, aliquet enim ut, consequat
            urna. Integer eget accumsan urna. Nulla ac mi ut nunc sagittis aliquet. Ut vitae ligula et nibh pharetra
            ullamcorper. In et ligula faucibus, sodales mauris id, lobortis odio. Maecenas rutrum, massa vitae
            scelerisque ultricies, risus nulla porta lacus, at imperdiet mauris orci sit amet erat. Nulla scelerisque ex
            sit amet mauris efficitur tempor. Donec fringilla neque bibendum eros tempus pellentesque. In eu placerat
            dui. Etiam elementum ac sapien eget molestie. Vestibulum ante ipsum primis in faucibus orci luctus et
            ultrices posuere cubilia Curae; Etiam a turpis at eros vehicula gravida. Praesent vestibulum eget ligula et
            varius. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
        </p>
        <p>
            Donec tincidunt odio non diam lobortis, tempus tempus sem faucibus. Aenean sed enim mollis, elementum elit
            non, blandit lacus. Maecenas condimentum fermentum lacus eu feugiat. Sed dictum risus est, eu condimentum
            augue laoreet at. Nullam non imperdiet est, vitae vulputate tellus. Duis vulputate tortor neque, nec
            hendrerit magna venenatis at. Etiam egestas nisl et purus molestie sollicitudin. Nullam id odio dui. Orci
            varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed lacinia tincidunt
            lorem eu aliquet. Morbi interdum eu nunc et blandit. Aenean non diam eros. In hac habitasse platea dictumst.
            Duis et interdum tortor. Ut eget ipsum vel odio blandit vestibulum.
        </p>
        <p>
            Integer sed sagittis lectus. Phasellus mattis, augue rutrum ultrices faucibus, justo lacus tincidunt diam,
            non finibus elit orci quis sapien. Fusce nulla risus, pellentesque vitae magna ut, pharetra lacinia urna.
            Cras mollis magna at viverra pulvinar. Nunc id bibendum justo, eget pharetra tortor. Integer finibus id est
            eget congue. Proin lobortis feugiat orci, vel egestas odio vulputate et. Proin et imperdiet justo, et
            feugiat risus. Mauris lobortis facilisis molestie. Aenean tellus risus, gravida quis turpis et, fringilla
            iaculis nibh. Ut quam sapien, ultricies vitae convallis rhoncus, tincidunt in massa. Cras scelerisque tellus
            metus, ut finibus nulla scelerisque in. Pellentesque fermentum nulla et feugiat cursus.
        </p>
        <p>
            Quisque mollis sem mauris, et malesuada sapien ultricies eget. Sed commodo felis fringilla, condimentum
            risus et, porttitor erat. Aliquam et auctor eros. Vestibulum id commodo ex. Integer et ornare erat.
            Phasellus dolor enim, efficitur a aliquet quis, ornare at velit. Sed auctor suscipit lobortis. Nullam id
            orci libero. Aliquam dolor mi, ultricies a velit ac, pretium consectetur orci. In varius nisi sem, ut
            vestibulum lacus fringilla eget. Ut sollicitudin lobortis mauris ut elementum. Suspendisse molestie nibh
            felis, eu aliquam neque commodo nec. Praesent eu varius risus, sit amet iaculis nibh. Pellentesque iaculis
            massa mauris, vel iaculis nulla tempus ut. Suspendisse feugiat velit vitae nulla venenatis hendrerit. Ut
            tortor diam, bibendum ac neque id, suscipit suscipit leo.
        </p>
        <p>
            Vestibulum quis mi quis ipsum egestas consectetur sit amet vitae odio. Quisque malesuada dui eu libero
            vulputate, sed interdum ex ornare. Quisque non mi tristique, laoreet libero eu, dictum ipsum. Nullam vel
            libero congue, auctor velit vel, fringilla erat. Cras nec semper augue. Phasellus vitae odio ornare tortor
            feugiat aliquam rutrum eget justo. Sed interdum fringilla mauris, dictum sollicitudin lectus condimentum at.
        </p>
    </article>

@endsection
