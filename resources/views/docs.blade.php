@extends('layout')

@section('content')
    <article>
        <h1>
            Document all things!
        </h1>
        <p>
            We strive to make 100% documented code.
        </p>
    </article>
@endsection

@section('title', 'Documentation')
