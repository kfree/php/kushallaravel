@extends('layout')

@section('title', 'Fork all things!')

@section('content')
    <article>
        <h1>
            Gitlab is our home
        </h1>
        <p>
            Kushal Laravel Project is proud to be on
            <a href="https://gitlab.com/kfree/php/kushallaravel">Gitlab</a>.
            Gitlab is an open-core project.
            Verizon is a notable commercial client.
            Gnome is a notable FOSS user as well.
            In any case, feel free to visit the gitlab repository for this project.
        </p>
    </article>
@endsection
