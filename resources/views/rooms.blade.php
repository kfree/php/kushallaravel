@extends('layout')

@section('content')

    @foreach($rooms as $room)
        {{ ucfirst($room->name) }}.
    @endforeach
@endsection
