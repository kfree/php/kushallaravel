<?php

namespace App\Http\Controllers;

class RoomController extends Controller
{
    public function home()
    {
        $rooms = \App\Room::all();
        return view('rooms', [
            'rooms' => $rooms
        ]);
    }
}
