<?php

namespace App\Http\Controllers;

class PagesController extends Controller
{
    //
    public function home()
    {
        $tasks = [
            "go to the store",
            "go to the market",
            "go to work"
        ];
        return view('welcome', [
            'tasks' => $tasks
        ]);
    }

    public function contact()
    {
        return view('contact');
    }

    public function docs()
    {
        return view('docs');
    }

    public function gitlab()
    {
        return view('gitlab');
    }
}
